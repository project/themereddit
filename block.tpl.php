
<div id="<?php print $block_html_id; ?>" class="<?php print $classes ?>" <?php print $attributes; ?>>
  <div class="gutter clearfix">
    <?php if (isset($edit_links)): ?>
    <?php print $edit_links; ?>
    <?php endif; ?>

    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
    <h2 class="title block-title" <?php print $title_attributes; ?>>
      <?php print $block->subject ?>
    </h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>

    <div class="content clearfix" <?php print $content_attributes; ?>>
      <?php print $content ?>
    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->

